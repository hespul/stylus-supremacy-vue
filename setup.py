from distutils.core import setup

setup(
    name='Stylus-supremacy for vue',
    version='0.3dev',
    # packages=['',],
    license='WTFPL',
    python_requires='>=3.6',
    py_modules=['stylus_supremacy_vue_formatter'],
    entry_points={
        "console_scripts": [
            "stylus_supremacy_vue_formatter=stylus_supremacy_vue_formatter:main",
        ]
    },
)
