import subprocess
import argparse


"""
Stylus-supremacy does not accept to take --options-file argument before file
"""


class StylusFormatter:

    def __init__(self, options_file=None):
        self.options_command = ["--options", options_file] if options_file else []

    def run(self, stylus_file):
        subprocess.run(
            [
                "stylus-supremacy",
                "format",
                stylus_file,
                "--replace",
                *self.options_command,
            ],
            check=True,
        )
        

def main(argv=None):

    try:
        # PARSE ARGUMENTS
        parser = argparse.ArgumentParser()
        parser.add_argument("--options-file", default="", help="Stylus supremacy options file")
        parser.add_argument("filenames", nargs="*", help="Stylus files to fix")
        args = parser.parse_args(argv)

        # Init formatter
        stylus_formatter = StylusFormatter(args.options_file)

        # Browse each passed vue files
        for stylus_file in args.filenames:
            print(stylus_file)
            stylus_formatter.run(stylus_file)

    except Exception as e:
        print(e)
        return 1
    
    return 0


if __name__ == '__main__':
    exit(main())
