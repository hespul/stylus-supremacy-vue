# Stylus-supremacy for vue

This little python 3 script take vue files and apply stylus-supremacy formater on style part (`<style lang="stylus">..</style>`)

This is intended to be used with pre-commit

## Usage

```bash
python stylus_supremacy_vue_formatter.py vue_file.vue my_vue_component.vue
python stylus_supremacy_vue_formatter.py --options-file .stylus_supremacy.json vue_file.vue my_vue_component.vue
```

## Pre-commit
```yaml
-   repo: https://bitbucket.org/hespul/stylus-supremacy-vue.git
    rev: '0.3'
    hooks:
    - id: stylus-supremacy-vue
      args: [--options-file, .stylus_supremacy.json]
```
