import re
import os
import sys
from tempfile import NamedTemporaryFile
import subprocess
import argparse


"""
Stylus-supremacy does not accept to parse direct .vue files.
This script take one or many .vue files and format themes using stylus-supremacy
"""

class StylusFormatter:

    stylus_tags_regex = r"(<style .*?lang=.stylus..*?>)(.*?)(</style>)"

    def __init__(self, options_file=None):
        self.options_command = ["--options", options_file] if options_file else []

    def format_stylus(self, stylus_content):

        temp_stylus_file = NamedTemporaryFile(mode="w", suffix=".styl", delete=False)
        temp_stylus_file.write(stylus_content)
        temp_stylus_file.close()

        subprocess.run(
            [
                "stylus-supremacy",
                "format",
                temp_stylus_file.name,
                "--replace",
                *self.options_command,
            ],
            check=True,
        )

        with open(temp_stylus_file.name, "r") as temp_stylus_file:
            formatted_stylus_content = temp_stylus_file.read()

        os.unlink(temp_stylus_file.name)

        return formatted_stylus_content

    def regex_sub_replacer(self, match):
        return "".join(
            (
                match.group(1), 
                self.format_stylus(match.group(2)), 
                match.group(3)
            )
        )

    def format(self, vue_content):
        return re.sub(
            self.stylus_tags_regex,
            self.regex_sub_replacer,
            vue_content,
            flags=re.DOTALL,
        )


def main(argv=None):

    try:
        # PARSE ARGUMENTS
        parser = argparse.ArgumentParser()
        parser.add_argument("--options-file", default="", help="Stylus supremacy options file")
        parser.add_argument("filenames", nargs="*", help="Vue files to fix")
        args = parser.parse_args(argv)

        # Init formatter
        stylus_formatter = StylusFormatter(args.options_file)

        # Browse each passed vue files
        for vue_file in args.filenames:
            print(vue_file)
            
            # Get vue file content
            with open(vue_file, encoding="UTF-8") as f:
                contents = f.read()

            # Format stylus and get new vue file content
            contents_with_formatted_stylus = stylus_formatter.format(contents)

            # Write new content to same file
            if (
                contents_with_formatted_stylus
                and contents_with_formatted_stylus != contents
            ):
                with open(vue_file, mode="w", encoding="UTF-8") as f:
                    f.write(contents_with_formatted_stylus)

    except Exception as e:
        print(e)
        return 1
    
    return 0

if __name__ == '__main__':
    exit(main())
